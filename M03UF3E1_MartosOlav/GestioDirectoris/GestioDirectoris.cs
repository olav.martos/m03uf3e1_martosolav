﻿/*
* AUTHOR: Olav Martos
* DATE: 24/01/2023
* DESCRIPTION: Conjunt d'exercicis per practicar la Gestio de Directoris amb C#
*/

using System;
using System.IO;
using System.Threading;

namespace GestioDirectoris
{
    class GestioDirectoris
    {
        /// <summary>
        /// Funcio Main d'aquest programa.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var menu = new GestioDirectoris();
            menu.Menu();
        }

        /// <summary>
        /// Menu dels exercicis
        /// </summary>
        public void Menu()
        {
            var option = "";
            string[] exercise = { "Sortir del menu", "Ex1: IsFileOrDirectory", "Ex2: DirectoryList", "Ex3: CreateDropUpdate", "Ex4: AllDirectory" };
            do
            {
                Console.Clear();
                showExercise(exercise);
                Console.Write("Escull una opcio: ");

                option = Console.ReadLine();

                // Ruta del directori on es van a executar els exercicis. Si vols canviar el directori, sols tens que canviar aquesta linea
                string path = @"f:/cs/pruebas/";

                switch (option)
                {
                    case "1":
                        Console.Clear();
                        programExecuted(exercise, option);
                        IsFileOrDirectory(path);
                        break;
                    case "2":
                        Console.Clear();
                        programExecuted(exercise, option);
                        DirectoryList(path);
                        break;
                    case "3":
                        Console.Clear();
                        programExecuted(exercise, option);
                        CreateDropUpdate(path);
                        break;
                    case "4":
                        Console.Clear();
                        programExecuted(exercise, option);
                        AllDirectory(path);
                        break;
                    case "0":
                        Console.Clear();
                        endExercise();
                        Environment.Exit(0);
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opció incorrecta!");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
                Console.ReadLine();
            } while (option != "0");

        }

        /// <summary>
        /// Mostra els exercicis per fila i enumerats.
        /// </summary>
        /// <param name="ex">Array de strings que contenen el nom dels exercicis</param>
        public void showExercise(string[] ex)
        {
            for (int i = 0; i < ex.Length; i++) Console.WriteLine($"{i}. - {ex[i]}");
        }

        /// <summary>
        /// Programa que es mostra sempre que un programa termina.
        /// </summary>
        public void endExercise()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa una tecla per terminar");
            Console.ForegroundColor = ConsoleColor.White;
            return;
        }

        /// <summary>
        /// Programa que ens permet saber que exercici hem executat
        /// </summary>
        /// <param name="exercise">Llista de exercicis pasats</param>
        /// <param name="option">Numero del exericio que l'usuari ha escrit</param>
        public void programExecuted(string[] exercise, string option)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            int index = Convert.ToInt32(option);
            Console.Write("Programa executat: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{exercise[index]}");
            Console.ResetColor();
        }





        /// <summary>
        /// Exercici 1: Funció que demana a l'usuari el nom d'un fitxer o directori. Indica si existeix o no.
        /// En cas d'existir, dira si es tracta d'un fitxer o d'un directori junt a la seva ruta absoluta.
        /// </summary>
        public void IsFileOrDirectory(string path)
        {
            Console.Write("Escriu el nom d'un fitxer o un directori: ");
            string name = Console.ReadLine();
            path += name;

            FileOrDirectoryExists(path);

            endExercise();
        }

        /// <summary>
        /// Part del IsFileOrDirectory()
        /// Comprova si aquest directori existeix i si existeix ens diu la seva ruta absoluta
        /// </summary>
        /// <param name="path">Ruta absoluta del fitxer/directorio</param>
        public void FileOrDirectoryExists(string path)
        {
            if (Directory.Exists(path) || File.Exists(path))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Ya existeix");
                Console.ResetColor();
                if (Directory.Exists(path)) Console.WriteLine($"<DIR> \t {path}");
                else Console.WriteLine($"<FILE> \t {path}");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("No existeix\n");
                Console.ResetColor();
            }
        }





        /// <summary>
        /// Programa que demana a l'usuari un directori. Mostra un llistat de carpetes i fitxer del directori, com cadenes.
        /// com carpetes i com fitxer, indicant si es tracta d'un directori o d'un fitxer.
        /// </summary>
        public void DirectoryList(string path)
        {
            Console.Write("Escriu el nom d'un directori: ");
            string name = Console.ReadLine();
            path += name;

            string t = "Procesant...\n";
            string f = "Aquest directori no existeix. Li pedim, de la forma més amable possible i sense ofendre a ningu que revisi la ruta del directori\n" +
                    "I si no existeix, el creeu el directori.\n" +
                    "\nCordialment, una CPU cansada de traballar." /*+
                    "\nSi voste poseeix la creencia de que les maquinas no traballem tant com els humans, li pedim que es golpegui el cap contra la pared fins que vegui marcians voltant seu"
                    */;

            if (Existence(path, t, f))
            {
                showList(path);
            }
            else Console.WriteLine("No existe");

            endExercise();
        }

        /// <summary>
        /// Mostra la llista dels fitxers de la ruta escrita per l'usuari en la funcio de DirectoryList();
        /// </summary>
        /// <param name="path">Ruta absoluta del directori</param>
        public void showList(string path)
        {
            string[] folders = Directory.GetDirectories(path);
            string[] files = Directory.GetFiles(path);

            foreach (string d in folders) Console.WriteLine(d);
            foreach (string f in files) Console.WriteLine(f);
            Console.WriteLine("\n---------------------------------------------\n\t   Que es cada cosa\n---------------------------------------------\n");
            foreach (string d in folders) Console.WriteLine("<DIR> " + Path.GetFileName(d));
            foreach (string f in files) Console.WriteLine("<FILE> " + Path.GetFileName(f));
        }





        /// <summary>
        /// Demanem a l'usuari un directori origen i hem de preguntar si volem crear-lo, esborrar-lo i/o canviar-li el nom
        /// </summary>
        public void CreateDropUpdate(string path)
        {
            string[] exercise = { "Salir", "Crear-lo", "Esborrar-lo", "Canviar-li el nom\n" };
            Console.Write("Escriu el nom d'un directori: ");
            string name = Console.ReadLine();
            path += name;

            Console.WriteLine($"Esta treballant sobre la ruta: {path}");


            Console.WriteLine("\n");
            showExercise(exercise);
            Console.Write("Que desitja fer: ");

            string option = Console.ReadLine();
            while (option != "0")
            {
                switch (option)
                {
                    case "1":
                        CreateDirectory(path);
                        break;
                    case "2":
                        DeleteDirectory(path);
                        break;
                    case "3":
                        path = RenameDirectory(path);
                        break;
                }
                Thread.Sleep(2000);
                Console.Clear();

                Console.WriteLine($"Esta treballant sobre la ruta: {path}");
                Console.WriteLine("\n");
                showExercise(exercise);
                Console.Write("Que desitja fer: ");
                option = Console.ReadLine();
            }

            endExercise();
        }

        /// <summary>
        /// Comprova si existeix el directori. En cas positiu mostra un missatge i retorna true. En cas negatiu, mostrara un altre missatge
        /// i retornara false.
        /// </summary>
        /// <param name="path">Ruta absoluta del directori a analitzar</param>
        /// <param name="t">Missatge passat, en cas de que existeixi.</param>
        /// <param name="f">Missatge passat, en cas de que no existeixi.</param>
        /// <returns></returns>
        public bool Existence(string path, string t, string f)
        {
            if (Directory.Exists(path))
            {
                Console.WriteLine(t);
                return true;
            }
            else
            {
                Console.WriteLine(f);
                return false;
            }
        }

        /// <summary>
        /// <para>Pasara al bool Existence() si la ruta existeix, en cas de que ens retorni false, ens creara el directori y mostrara el missatge
        /// "Se ha creado".</para>
        /// <para>Si Existence() ha retornat true, dira que ya existia i no fara res.</para>
        /// </summary>
        /// <param name="path">Ruta absoluta del directori</param>
        public void CreateDirectory(string path)
        {
            // string t = "La materia no es crea ni es destrueix, es transforma, excepte en aquest cas que la carpeta ja estaba creada";
            string t = "No es pot crear una cosa que ja havia sigut creada amb aterioritat";
            string f = "S'ha creat amb una tasa de corrupcio del 99.9999%\nFelicitats el resultat ha sigut del 0.0001% restant";
            if (!Existence(path, t, f))
            {
                Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// <para>Pasara al bool Existence() si la ruta existeix, en caso de que ens retorni true, ens borrara el directori i mostrara el missatge
        /// "Se ha borrado"</para>
        /// <para>En cas de que retroni false dira que no es pot borrar algo que no existeix.</para>
        /// </summary>
        /// <param name="path">Ruta absoluta del directori</param>
        public void DeleteDirectory(string path)
        {
            string t = "S'ha borrat el directori C:/Windows/System32 amb un 100% d'exito.";
            string f = "No es pot esborrar alguna cosa que no existeix";
            if (Existence(path, t, f))
            {
                Directory.Delete(path);
            }
        }

        /// <summary>
        /// Comprovara si el directori existeix i preguntara qual es el nou nom que li vols possar.
        /// En cas de no existir mostrara un missatge d'error
        /// </summary>
        /// <param name="path">Ruta absoluta</param>
        /// <returns>Retorna la ruta absoluta del directori tant si ha sigut modificada com si no.</returns>
        public string RenameDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                Console.Write("Qual es el nou nom: ");
                string newName = Console.ReadLine();
                string newPath = @"f:/cs/pruebas/" + newName;
                Directory.Move(path, newPath);
                path = newPath;
                Console.WriteLine("S'ha modificat el nom del directori i del propietari de la teva compte bancaria.");
            }
            else Console.WriteLine("No es pot modificar el nom d'alguna cosa que no existeix");
            return path;
        }





        /// <summary>
        /// Mostra tot el directori
        /// </summary>
        public void AllDirectory(string path)
        {

            Console.Write("Escriu el nom del directori: ");
            string name = Console.ReadLine();

            path += name;

            string t = "Informacio de " + path + "\n\n";
            string f = "No existeix";
            Console.WriteLine("");

            if (Existence(path, t, f))
            {
                Dires(path);
                Console.WriteLine("\n");
                Files(path);
            }
            endExercise();
        }

        /// <summary>
        /// Mostra els directoris i subdirectoris pero sol mostra el nom i la data de l'ultima modificacio
        /// </summary>
        /// <param name="path">Ruta absoluta del directori</param>
        void Dires(string path)
        {
            string[] dirs = Directory.GetDirectories(path, "*", SearchOption.AllDirectories);
            foreach (string dir in dirs) Console.WriteLine($"{Path.GetFileName(dir)}/\t\t{Directory.GetLastWriteTime(dir)}");
        }

        /// <summary>
        /// Mostra TOTS els fitxers del directori pero sols mostra el nom, el tamany i la data de l'ultima modificacio
        /// </summary>
        /// <param name="path">Ruta absoluta del directori</param>
        void Files(string path)
        {
            string[] files = Directory.GetFiles(path, "*", SearchOption.AllDirectories);
            foreach (string fil in files)
            {
                FileInfo tamany = new FileInfo(fil);
                Console.WriteLine($"{Path.GetFileName(fil)}\t\t{tamany.Length} bytes\t{Directory.GetLastWriteTime(fil)}");
            }
        }
    }
}